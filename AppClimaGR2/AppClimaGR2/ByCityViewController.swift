//
//  ByCityViewController.swift
//  AppClimaGR2
//
//  Created by Sebastian Guerrero on 11/6/17.
//  Copyright © 2017 SG. All rights reserved.
//

import UIKit

class ByCityViewController: UIViewController {
  
  //MARK:- Outlets
  
  @IBOutlet weak var cityTextField: UITextField!
  
  @IBOutlet weak var weatherLabel: UILabel!
  
  //MARK:- ViewController lifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
  }
  
  //MARK:- Actions
  
  @IBAction func consultarButtonPressed(_ sender: Any) {
    getWeatherByCity(cityTextField.text!)
  }
  
  private func getWeatherByCity(_ city:String){
    let service = Servicio()
    service.getWeatherByCity(city: city) { (weather) in
      DispatchQueue.main.async {
        self.weatherLabel.text = weather
      }
    }
  }
}
