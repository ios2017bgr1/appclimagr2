//
//  ByLocationViewController.swift
//  AppClimaGR2
//
//  Created by Sebastian Guerrero on 10/27/17.
//  Copyright © 2017 SG. All rights reserved.
//

import UIKit
import CoreLocation

class ByLocationViewController: UIViewController, CLLocationManagerDelegate {
  
  let locationManager = CLLocationManager()
  var didGetWheather = false
  
  //MARK:- Outlets
  @IBOutlet weak var weatherLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    locationManager.requestWhenInUseAuthorization()
    
    if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
      locationManager.delegate = self
      locationManager.startUpdatingLocation()
    }
    
  }
  
  //MARK:- CLLocationManagerDelegate
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    let location = manager.location?.coordinate
    if !didGetWheather {
      getWeatherByLocation(
        lat: (location?.latitude)!,
        lon: (location?.longitude)!
      )
      didGetWheather = true
    }
  }
  
  //MARK:- Actions
  
  @IBAction func salirButtonPressed(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
  private func getWeatherByLocation(lat:Double, lon:Double){
    let servicio = Servicio()
    servicio.getWeatherByLocation(lat: lat, lon: lon) { (weather) in
      DispatchQueue.main.async {
        self.weatherLabel.text = weather
      }
    }
  }
}




